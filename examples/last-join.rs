extern crate wynncraft;

extern crate chrono;
use chrono::prelude::*;

use std::env;

fn main() {
	let args = env::args().collect::<Vec<_>>();
	match args.get(1).map(|x| &**x) {
		Some("player-lastjoin") if args.len() > 2 => {
			for name in &args[2..] {
				println!("{}: {:?}", name, wynncraft::player(name).unwrap().meta.lastJoin);
			}
		},
		Some("guild-player-lastjoin") if args.len() > 2 => {
			let mut inactive = 0;
			for guild_name in &args[2..] {
				for name in wynncraft::guild(guild_name).unwrap().unwrap().members.iter().map(|x| &x.name) {
					let days = wynncraft::player(name)
						.map(|x| x.meta.lastJoin)
						.map(|x| x.parse::<DateTime<FixedOffset>>().unwrap())
						.map(|x| Local::now().signed_duration_since(x).num_days() as i64)
						.unwrap_or(-1);
					println!("{}: {:?} days ago",
						name,
						days
					);
					if days > 14 {
						inactive += 1;
					}
				}
			}
			println!("--> {} inactive", inactive);
		},
		_ => {} // TODO
	}
}
