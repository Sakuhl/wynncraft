extern crate reqwest;
use reqwest::blocking::get;

extern crate serde;
extern crate serde_json;
use serde_json::Value;
#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate failure;
use failure::Error;

pub fn player(name: &str) -> Result<Player, Error> {
	let resp = get(&format!("https://api.wynncraft.com/v2/player/{}/stats", name))?
		.error_for_status()?
		.text().unwrap();

	let x: Value = serde_json::from_str(&resp)?;
	//println!("{:?}", x);
	Ok(serde_json::from_value(x["data"][0].clone())?)
}

pub fn guild(name: &str) -> Result<Option<Guild>, Error> {
	let resp = get(&format!("https://api.wynncraft.com/public_api.php?action=guildStats&command={}", name))?
		.error_for_status()?
		.text().unwrap();

	if let Ok(error) = serde_json::from_str::<APIError>(&resp) {
		bail!("API error ({})", error.error);
	} else {
		Ok(serde_json::from_str(&resp)?)
	}
}

pub fn guild_list() -> Result<Vec<String>, Error> {
	let resp = get("https://api.wynncraft.com/public_api.php?action=guildList")?
		.error_for_status()?
		.text().unwrap();

	if let Ok(error) = serde_json::from_str::<APIError>(&resp) {
		bail!("API error ({})", error.error);
	} else {
		let list: GuildList = serde_json::from_str(&resp)?;
		Ok(list.guilds)
	}
}

pub fn guild_leaderboard() -> Result<Top100Guilds, Error> {
	let resp = get("https://api.wynncraft.com/public_api.php?action=statsLeaderboard&type=guild&timeframe=alltime")?.error_for_status()?.text()?;

	if let Ok(error) = serde_json::from_str::<APIError2>(&resp) {
		bail!("API error ({})", error.message);
	} else {
		let list: Top100Guilds = serde_json::from_str(&resp)?;
		Ok(list)
	}
}

#[derive(Deserialize)]
pub struct APIError {
	pub error: String
}

#[derive(Deserialize)]
pub struct APIError2 {
	pub code: u64,
	pub message: String
}

#[derive(Deserialize)]
pub struct GuildList {
	pub guilds: Vec<String>
}

#[derive(Deserialize)]
pub struct Top100Guilds {
	pub data: Vec<GuildEntry>
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GuildEntry {
	pub name: String,
	pub prefix: String,
  	pub xp: u64,
  	pub level: u64,
  	pub created: String,
  	pub territories: u64,
	pub members_count: u64,
	pub num: u64
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Guild {
	pub name: String,
	pub prefix: String,
  	pub members: Vec<Member>,
  	pub xp: f64,
  	pub level: u64,
  	pub created: String,
  	pub created_friendly: String,
  	pub territories: u64
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Member {
	pub name: String,
	pub rank: Option<Rank>,
	pub contributed: u64,
	pub joined: String,
	pub joined_friendly: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum Rank {
	Owner,
	Chief,
	Captain,
	Recruiter,
	Recruit
}

#[derive(Debug, Deserialize)]
#[allow(non_snake_case)]
pub struct Player {
	pub username: String,
	pub uuid: String,
	pub rank: String,
	pub meta: PlayerMetadata,
	pub classes: Vec<Class>,
	pub guild: PlayerGuildInfo,
	pub global: GlobalPlayerInfo,
	pub ranking: Value
}

#[derive(Debug, Deserialize)]
#[allow(non_snake_case)]
pub struct PlayerMetadata {
	pub firstJoin: String,
	pub lastJoin: String,
	pub location: Location,
	pub playtime: u64,
	pub tag: Value,
	pub veteran: bool
}

#[derive(Debug, Deserialize)]
pub struct Location {
	pub online: bool,
	pub server: Option<String>
}

#[derive(Debug, Deserialize)]
pub struct GlobalPlayerInfo {
	pub chestsFound: u64,
	pub blocksWalked: u64,
	pub itemsIdentified: u64,
	pub mobsKilled: u64,
	pub totalLevel: Value,
	pub pvp: Pvp,
	pub logins: u64,
	pub deaths: u64,
	pub discoveries: u64,
	pub eventsWon: u64
}

#[derive(Debug, Deserialize)]
pub struct Pvp {
	pub kills: u64,
	pub deaths: u64
}

#[derive(Debug, Deserialize)]
#[allow(non_snake_case)]
pub struct Class {
	pub name: String,
	pub level: u64,
	pub dungeons: Value,
	pub quests: Value,
	pub itemsIdentified: u64,
	pub mobsKilled: u64,
	pub pvp: Value,
	pub chestsFound: u64,
	pub blocksWalked: u64,
	pub logins: u64,
	pub deaths: u64,
	pub playtime: u64,
	pub gamemode: Value,
	pub skills: Value,
	pub professions: Value,
	pub discoveries: u64,
	pub eventsWon: u64,
	pub preEconomyUpdate: bool
}

#[derive(Debug, Deserialize)]
pub struct WFPlayerInfo {
	pub kills: u64,
	pub deaths: u64,
	pub wins: u64,
	pub losses: u64
}

#[derive(Debug, Deserialize)]
pub struct PlayerRankings {
	pub pvp: Option<u64>,
	pub player: Option<u64>,
	pub guild: Option<u64>
}

#[derive(Debug, Deserialize)]
pub struct PlayerGuildInfo {
	pub name: Option<String>,
	pub rank: Option<String>
}

#[cfg(test)]
mod tests {
	#[test]
	fn player() {
		::player("dukioooo").unwrap();
		::player("FallendeWurst").unwrap();
		::player("lmays11").unwrap();
	}

	#[test]
	fn guild() {
		::guild("HackForums").unwrap();
	}

	#[test]
	fn guild_leaderboard() {
		::guild_leaderboard().unwrap();
	}

	#[test]
	fn guild_list() {
		::guild_list().unwrap();
	}
}